# README #

### What is this repository for? ###

* A basic library that creates a decision tree model for making predictions
* This was created more to demonstrate my understanding of decision trees
* Version 0.0.1

### TO DO ###

* More work needs to be done on splitting.  Splits don't factor in Greater or Less Than.
* This means the algorithm is unable to solve numbers that haven't been learned.
* I would like to create bagging and boosting algorithms and apply to this one.
* Not able to install the library

### Who do I talk to? ###

* William Rogers
* wrrogers@hotmail.com